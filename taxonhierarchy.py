#! /usr/bin/env python3

import rdflib
from SPARQLWrapper import SPARQLWrapper, JSON


class TaxonHierarchyExtractor:

    def __init__(self, endpointURL):
        self._endpoint = SPARQLWrapper(endpointURL)
        self._graph = rdflib.Graph()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Extract a portion of the NCBI Taxon ontology as an RDF graph')
    parser.add_argument("endpointURL", help="URL (starting with 'http') of the SPARQL endpoint")	# 'http://localhost:3030/reactome/ncbiTaxon'
    args = parser.parse_args()
    extractor = TaxonHierarchyExtractor(args.endpointURL)
